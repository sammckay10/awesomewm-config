-- Require config files, order is important

require("config.startup")
require("config.theme")
require("config.autorun")
require("config.wibar")
require("config.layouts")
require("config.wallpapers")
require("config.keybindings")
require("config.client-signals")
require("config.rules")
require("awful.autofocus")
