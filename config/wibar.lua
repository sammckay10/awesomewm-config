local awful = require("awful")
local gears = require("gears")
local wibox = require("wibox")
local beautiful = require("beautiful")

local dpi = require("beautiful.xresources").apply_dpi

local volume_widget = wibox.widget.slider({
	bar_shape = gears.shape.rounded_rect,
	bar_height = dpi(2),
	forced_width = dpi(40),
	bar_color = beautiful.fg_normal,
	handle_color = beautiful.bg_focus,
	handle_shape = gears.shape.circle,
	handle_width = dpi(10),
	value = 70,
})

volume_widget:connect_signal("widget::redraw_needed", function(params)
	-- This needs to be the name of your output device, couldn't figure out how to grab it automatically
	-- You can run "amixer" to list your devices
	os.execute("amixer sset 'Speaker' " .. volume_widget.value .. "%")
end)

-- Sync up actual volume with what's displayed in the volume bar at startup
awful.spawn.once("amixer sset 'Speaker' 70%")

-- Loop through each screen
awful.screen.connect_for_each_screen(function(screen)
	-- Create a wibar
	screen.wibar = awful.wibar({
		position = "top",
		screen = screen,
		height = dpi(30),
		bg = beautiful.bg_normal .. "99",
		-- width = screen.geometry.width - (beautiful.useless_gap * 4),
		-- margins = {
		--     top = beautiful.useless_gap * 2,
		-- },
	})

	-- Add widgets to the wibar
	--
	-- This gets a bit messy, wibox spacing/styling options are not the best
	-- Enjoy the mess below
	screen.wibar:setup({
		-- Top level layout and spacing
		layout = wibox.layout.align.horizontal,
		spacing = 10,

		-- Left side group
		{
			layout = wibox.layout.align.horizontal,

			-- Taskbar
			awful.widget.tasklist({
				screen = screen,
				filter = awful.widget.tasklist.filter.currenttags,
				buttons = gears.table.join(awful.button({}, 1, function(client)
					client:emit_signal("request::activate", "tasklist", {
						raise = true,
					})
				end)),
				widget_template = {
					left = 10,
					widget = wibox.container.margin,
					{
						layout = wibox.layout.fixed.horizontal,
						{
							widget = wibox.container.margin,
							{
								id = "icon_role",
								widget = wibox.widget.imagebox,
							},
							margins = {
								top = 5,
								bottom = 5,
							},
						},
						{
							widget = wibox.container.margin,
							{
								id = "text_role",
								widget = wibox.widget.textbox,
							},
							margins = {
								top = 5,
								bottom = 5,
								left = 5,
							},
						},
					},
				},
			}),
		},

		-- Add space between left/right groups
		{
			layout = wibox.layout.fixed.horizontal,
			wibox.widget.separator({
				visible = false,
			}),
		},

		-- Right side group
		{
			layout = wibox.layout.fixed.horizontal,
			spacing = 10,

			-- Clock
			wibox.widget.textclock(),

			-- Separator
			wibox.widget.separator({
				orientation = "horizontal",
				forced_width = 2,
				thickness = 15,
				opacity = 0.5,
			}),

			-- Volume
			wibox.widget.textbox("Volume:"),
			volume_widget,

			-- Separator
			wibox.widget.separator({
				orientation = "horizontal",
				forced_width = 2,
				thickness = 15,
				opacity = 0.5,
			}),

			-- Tiling mode
			wibox.widget.textbox("Layout:"),
			awful.widget.layoutbox({
				screen = screen,
			}),

			-- Separator
			wibox.widget.separator({
				orientation = "horizontal",
				forced_width = 2,
				thickness = 15,
				opacity = 0.5,
			}),

			-- Tag list
			awful.widget.taglist({
				screen = screen,
				filter = awful.widget.taglist.filter.all,
				buttons = gears.table.join(awful.button({}, 1, function(tag)
					tag:view_only()
				end)),
			}),

			-- Faux padding right (empty so we just utilise the spacing between widgets)
			wibox.widget.separator({
				visible = true,
				forced_width = 0,
				color = "#00000000",
			}),
		},
	})
end)
