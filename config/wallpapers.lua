local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")

local function set_wallpaper(screen)
	if beautiful.wallpaper then
		local wallpaper = beautiful.wallpaper

		-- If wallpaper is a function, call it with the screen
		if type(wallpaper) == "function" then
			wallpaper = wallpaper(screen)
		end

		gears.wallpaper.maximized(wallpaper, screen)
	end
end

-- Re-set wallpaper when a screen's geometry changes
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(screen)
	-- Wallpaper
	set_wallpaper(screen)

	-- Each screen has its own tag table with default layout
	if screen.geometry.width >= screen.geometry.height then
		awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, screen, awful.layout.layouts[1])
	else
		awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, screen, awful.layout.layouts[2])
	end
end)
