local gears = require("gears")
local awful = require("awful")
local hotkeys_popup = require("awful.hotkeys_popup")

super = "Mod4"
alt = "Mod1"
control = "Control"
tab = "Tab"
shift = "Shift"

terminal = "alacritty"
editor = "nvim"

globalkeys = gears.table.join(
	-- Show keybindings
	awful.key({ super }, ",", hotkeys_popup.show_help, { description = "show help", group = "awesome" }),

	-- Move between tags
	awful.key({ control, alt }, "Left", awful.tag.viewprev, { description = "view previous", group = "tag" }),
	awful.key({ control, alt }, "Right", awful.tag.viewnext, { description = "view next", group = "tag" }),

	-- Take a screenshot (selection)
	awful.key({}, "Print", function()
		awful.spawn("scrot -s '/tmp/%F_%T_$wx$h.png' -e 'xclip -selection clipboard -target image/png -i $f'")
	end, { description = "take screenshot", group = "screen" }),

	-- Move window to next tag
	awful.key({ control, super, alt }, "Right", function()
		local t = client.focus and client.focus.first_tag or nil

		if t == nil then
			return
		end

		local tag = client.focus.screen.tags[(t.name % 9) + 1]

		awful.client.movetotag(tag)
		awful.tag.viewnext()
	end, { description = "move window to next tag", group = "tag" }),

	-- Move window to previous tag
	awful.key({ control, super, alt }, "Left", function()
		local t = client.focus and client.focus.first_tag or nil

		if t == nil then
			return
		end

		local tag = client.focus.screen.tags[(t.name - 2) % 9 + 1]

		awful.client.movetotag(tag)
		awful.tag.viewprev()
	end, { description = "move window previous next tag", group = "tag" }),

	-- Focus on next window
	awful.key({ alt }, tab, function()
		awful.client.focus.byidx(1)
	end, { description = "focus on next window", group = "client" }),

	-- Focus on previous window
	awful.key({ alt, shift }, tab, function()
		awful.client.focus.byidx(-1)
	end, { description = "focus on previous window", group = "client" }),

	-- Swap with window to right
	awful.key({ super }, "Right", function()
		awful.client.swap.global_bydirection("right")
	end, { description = "swap with right", group = "client" }),

	-- Swap with window to left
	awful.key({ super }, "Left", function()
		awful.client.swap.global_bydirection("left")
	end, { description = "swap with left", group = "client" }),

	-- Swap with window above
	awful.key({ super }, "Up", function()
		awful.client.swap.global_bydirection("up")
	end, { description = "swap with above", group = "client" }),

	-- Swap with window to below
	awful.key({ super }, "Down", function()
		awful.client.swap.global_bydirection("down")
	end, { description = "swap with below", group = "client" }),

	-- Focus on next screen
	awful.key({ super }, tab, function()
		awful.screen.focus_relative(1)
	end, { description = "focus on next screen", group = "screen" }),

	-- Open terminal
	awful.key({ super }, "Return", function()
		awful.spawn(terminal)
	end, { description = "open a terminal", group = "launcher" }),

	-- Reload awesome
	awful.key({ super, control }, "r", awesome.restart, { description = "reload awesome", group = "awesome" }),

	-- Quit awesome
	awful.key({ super, control }, "q", awesome.quit, { description = "quit awesome", group = "awesome" }),

	-- Increase master width
	awful.key({ super }, "minus", function()
		awful.tag.incmwfact(0.05)
	end, { description = "increase master width factor", group = "layout" }),

	-- Decrease master width
	awful.key({ super }, "equal", function()
		awful.tag.incmwfact(-0.05)
	end, { description = "decrease master width factor", group = "layout" }),

	-- Select next layout
	awful.key({ super }, "space", function()
		awful.layout.inc(1)
	end, { description = "select next", group = "layout" }),

	-- Open rofi
	awful.key({ super }, "p", function()
		awful.spawn(
			'rofi -color-window "argb:0ffffff, argb:0ffffff, argb:0ffffff" -color-normal "argb:0000000, #000000, argb:0000000, argb:0000000, #000000" -color-active "argb:0000000, #000000, argb:0000000, argb:0000000, #000000" -color-urgent "argb:0000000, #000000, argb:0000000, argb:0000000, #000000" -show run'
		)
	end, { description = "launch rofi", group = "launcher" })
)

clientkeys = gears.table.join(
	-- Make window fullscreen
	awful.key({ super }, "f", function(c)
		c.fullscreen = not c.fullscreen
		c:raise()
	end, { description = "toggle window fullscreen", group = "client" }),

	-- Close window
	awful.key({ control }, "q", function(c)
		c:kill()
	end, { description = "close window", group = "client" }),

	-- Also close window
	awful.key({ super }, "q", function(c)
		c:kill()
	end, { description = "close window", group = "client" }),

	-- Move window to next screen
	awful.key({ super }, "w", function(c)
		c:move_to_screen(c.screen.index + 1)
	end, { description = "move window to next screen", group = "screen" })
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
	globalkeys = gears.table.join(
		globalkeys,
		-- View tag only
		awful.key({ control, alt }, "#" .. i + 9, function()
			local screen = awful.screen.focused()
			local tag = screen.tags[i]
			if tag then
				tag:view_only()
			end
		end, { description = "focus on tag #" .. i, group = "tag" }),

		-- Move client to tag
		awful.key({ control, super, alt }, "#" .. i + 9, function()
			if client.focus then
				local tag = client.focus.screen.tags[i]
				if tag then
					client.focus:move_to_tag(tag)
					tag:view_only()
				end
			end
		end, { description = "move window to tag #" .. i .. " and focus on that tag", group = "tag" })
	)
end

-- Set keys
root.keys(globalkeys)
