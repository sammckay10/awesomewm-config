-- Beautiful docs: https://awesomewm.org/doc/api/libraries/beautiful.html

local beautiful = require("beautiful")
local gears = require("gears")
local dpi = require("beautiful.xresources").apply_dpi

local theme = {}

-- Colors
-- Have fun time at https://coolors.co/generate
local white = "#FCFAF9"
local primary = "#333333"
local accent = "#48E5C2"
local transparent = "#00000000"

-- Font
theme.font = "Quicksand Bold 9"

-- Foreground colors
theme.fg_normal = white

-- Background colors
theme.bg_normal = primary
theme.bg_focus = accent

-- Border colors
theme.border_normal = theme.bg_normal
theme.border_focus = theme.bg_focus

-- Gaps
theme.useless_gap = dpi(5)

-- Border width
theme.border_width = dpi(3)

-- Notifications
theme.notification_icon_size = dpi(30)

-- Taglist
theme.taglist_fg_empty = theme.fg_normal .. "99"
theme.taglist_fg_focus = theme.bg_focus
theme.taglist_fg_occupied = theme.fg_normal
theme.taglist_bg_normal = transparent
theme.taglist_bg_focus = transparent

-- Tasklist
theme.tasklist_fg_normal = theme.fg_normal
theme.tasklist_fg_focus = theme.bg_focus
theme.tasklist_bg_normal = transparent
theme.tasklist_bg_focus = theme.bg_focus
theme.tasklist_disable_task_name = true

-- Wallpaper
theme.wallpaper = gears.filesystem.get_configuration_dir() .. "wallpapers/wavey.png"

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = nil

beautiful.init(theme)
