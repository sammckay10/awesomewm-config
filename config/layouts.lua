local awful = require("awful")

awful.layout.suit.tile.name = "X"
awful.layout.suit.max.name = "F"

awful.layout.layouts = {
	awful.layout.suit.tile,
	{
		name = "Y",
		arrange = function(window)
			local area = window.workarea

			for index, value in pairs(window.clients) do
				window.geometries[value] = {
					x = area.x,
					y = area.y + (index - 1) * (area.height / #window.clients),
					width = area.width,
					height = area.height / #window.clients,
				}
			end
		end,
	},
	awful.layout.suit.max,
}
