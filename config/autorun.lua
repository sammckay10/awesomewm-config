local awful = require("awful")
local date = os.date("*t", os.time())

-- Disable caps lock
awful.spawn.once("setxkbmap -option ctrl:nocaps")

-- Set mouse speed for wired/wireless Pulsar X2 Mini
awful.spawn.once("xinput --set-prop 20 'libinput Accel Speed' -1")
awful.spawn.once("xinput --set-prop 10 'libinput Accel Speed' -1")

-- Fix display location, rotation and scale
awful.spawn.once(
	"xrandr --output DP-0 --primary --mode 1920x1080 --pos 0x840 --rotate normal --output DP-4 --mode 2560x1440 --pos 1920x0 --rotate right --scale 0.75x0.75"
)

-- Start compositor
awful.spawn.once("picom")

-- Start lxsession policy kit
awful.spawn.once("lxpolkit")

-- Run some apps automatically (probs slack) if within working hours
if date.hour < 18 and date.hour > 7 and date.wday ~= 7 and date.wday ~= 1 then
	awful.spawn.once("slack")
end
