local awful = require("awful")
local gears = require("gears")
local beautiful = require("beautiful")

clientbuttons = gears.table.join(
	awful.button({}, 1, function(c)
		c:emit_signal("request::activate", "mouse_click", { raise = true })
	end),

	awful.button({ "Mod4" }, 1, function(c)
		c:emit_signal("request::activate", "mouse_click", { raise = true })
		awful.mouse.client.move(c)
	end),

	awful.button({ "Mod4" }, 3, function(c)
		c:emit_signal("request::activate", "mouse_click", { raise = true })
		awful.mouse.client.resize(c)
	end)
)

awful.rules.rules = {
	-- Rules to apply to new clients (through the "manage" signal).
	{
		rule = {},
		properties = {
			border_width = beautiful.border_width,
			border_color = beautiful.border_normal,
			focus = awful.client.focus.filter,
			raise = true,
			keys = clientkeys,
			buttons = clientbuttons,
			screen = awful.screen.preferred,
			placement = awful.placement.no_overlap + awful.placement.no_offscreen,
		},
	},
	-- Add titlebars to normal clients and dialogs
	{
		rule_any = {
			type = {
				"normal",
				"dialog",
			},
		},
		properties = { titlebars_enabled = false },
	},
	-- Always open some windows on last screen
	{
		rule_any = {
			instance = {
				"slack",
			},
			role = {
				"pop-up",
			},
		},
		properties = {
			screen = function()
				return screen:count()
			end,
		},
	},
}
